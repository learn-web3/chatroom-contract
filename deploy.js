const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const { abi, bytecode } = require("./compile");
require("dotenv").config();

const provider = new HDWalletProvider(process.env.MNEMONIC, process.env.INFURA);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();
  console.log("Account", accounts[0]);
  const result = await new web3.eth.Contract(abi)
    .deploy({ data: bytecode })
    .send({ gas: "3000000", from: accounts[0] });

  console.log("Address", result.options.address);
  console.dir(abi, { depth: null });
};

deploy();
