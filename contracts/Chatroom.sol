// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

contract Chatroom {
    struct Profile {
        string name;
        bool registered;
    }

    struct Message {
        string name;
        string text;
        uint256 timestamp;
        uint256 number;
    }

    string[] names;
    address[] addresses;
    Message[] messages;

    mapping(address => Profile) private addressProfiles;
    mapping(string => address) private namesAddresses;

    function signup(string calldata _nickname) public {
        require(
            strLen(_nickname) > 2,
            "Nickname length should be more than 3 symbols"
        );
        require(
            addressProfiles[msg.sender].registered == false,
            "You are already registered"
        );
        require(
            namesAddresses[_nickname] == address(0),
            "This nickname is already picked"
        );
        names.push(_nickname);
        addresses.push(msg.sender);
        namesAddresses[_nickname] = msg.sender;
        addressProfiles[msg.sender].name = _nickname;
        addressProfiles[msg.sender].registered = true;
    }

    function getUser() public view returns (Profile memory) {
        return addressProfiles[msg.sender];
    }

    function namesList() public view returns (string[] memory) {
        return names;
    }

    function send(string calldata _text) public {
        require(
            addressProfiles[msg.sender].registered == true,
            "You are not registered yet"
        );
        string memory name = addressProfiles[msg.sender].name;
        messages.push(Message(name, _text, block.timestamp, block.number));
    }

    /* function getMessages(uint256 cursor, uint256 limit)
        public
        view
        returns (Message[] memory, uint256)
    {
        uint256 length = limit;
        if (length > messages.length - cursor) {
            length = messages.length - cursor;
        }
        Message[] memory page = new Message[](length);
        uint256 internalCursor = cursor;
        if (cursor == 0) {
            internalCursor = messages.length - length;
        } else {
            internalCursor = cursor - 1;
        }
        for (uint256 i = 0; i < length; i++) {
            page[i] = messages[internalCursor + i];
        }
        uint256 newCursor = internalCursor;
        if (page.length > internalCursor) newCursor = 1;
        else newCursor = internalCursor - page.length;
        return (page, newCursor);
    } */

    function getMessages(int256 cursor)
        public
        view
        returns (Message[] memory, uint256)
    {
        uint256 length = 5;
        uint256 intCursor;

        if (cursor == -1) {
            intCursor = messages.length - length;
        } else {
            intCursor = uint256(cursor);
        }

        if (length > messages.length - intCursor) {
            length = messages.length - intCursor;
        }

        Message[] memory page = new Message[](length);

        for (uint256 i = 0; i < length; i++) {
            page[i] = messages[intCursor + i];
        }

        uint256 newCursor = intCursor;
        if (page.length > intCursor) newCursor = 0;
        else newCursor = intCursor - page.length;
        return (page, newCursor);
    }

    function strLen(string memory s) private pure returns (uint256) {
        return bytes(s).length;
    }
}
